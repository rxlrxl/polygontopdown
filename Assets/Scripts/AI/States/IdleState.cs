using ShoriGames.HFSM;
using UnityEngine;

namespace PolygonTopDown.States
{
    public class IdleState : TopDownStateBase
    {
        private readonly CharacterMovementBehaviour _movementBehaviour;
        
        private Vector3 _startingPosition = Vector3.zero;
        private Vector3 _targetPosition = Vector3.zero;
        
        public IdleState(Character character, Character player) : base(character, player)
        {
            _movementBehaviour = Character.GetComponent<CharacterMovementBehaviour>();
        }

        public override void OnStateEnter(IState fromState)
        {
            base.OnStateEnter(fromState);
            
            _startingPosition = Character.transform.position;
            
            var direction = Random.onUnitSphere;
            direction.y = 0.0f;
            direction.Normalize();
            
            _targetPosition = _startingPosition + direction * 5.0f;
        }

        public override void Update()
        {
            base.Update();

            if ((_targetPosition - Character.transform.position).magnitude > 1.0f)
            {
                MoveToPosition(_targetPosition);
            }
            else
            {
                MoveToPosition(_startingPosition);
            }
        }

        private void MoveToPosition(Vector3 position)
        {
            var movementVector = (position - Character.transform.position).normalized;
            _movementBehaviour.ChangeCharacterMovementDirection(movementVector);
        }
    }
}
