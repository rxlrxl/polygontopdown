using PolygonTopDown.States;
using ShoriGames.HFSM;
using UnityEngine;

namespace PolygonTopDown
{
    public static class AIStateMachineFactory
    {
        public static StateMachine CreateDefaultStateMachine(Character character)
        {
            var player = GameObject.FindObjectOfType<PlayerCharacterController>().Character;
            
            var idleState = new IdleState(character, player);
            
            var idleToExitTransition = new DelegateCheckTransition<IdleState, StateMachine.ExitState>(
                (state, exitState) => false,
                idleState,
                new StateMachine.ExitState());
            
            return StateMachine.Create(idleState);
        }
    }
}
