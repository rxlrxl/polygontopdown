﻿using System.Linq;
using UnityEngine;

namespace PolygonTopDown
{
    public class Weapon : InventoryItem
    {
        [SerializeField] public WeaponType Type = WeaponType.Undefined;
        [SerializeField] public Transform LeftHandIKTarget = null;

        [SerializeField] private Transform _shootingPoint = null;

        [SerializeField] private ParticleSystem _muzzleFlashParticleSystem = null;

        public override void Drop(Vector3 dropPosition)
        {
            if (_visualModel.gameObject.activeSelf) // If weapon is in hands now
            {
                _visualModel.SetParent(transform);
            }
            
            base.Drop(dropPosition);
        }

        public override void Apply(Character character)
        {
            _visualModel.gameObject.SetActive(true);
            
            _visualModel.SetParent(character.RightHandBone);
            _visualModel.localPosition = Vector3.zero;
            _visualModel.localRotation = Quaternion.identity;
            
            character.ApplyWeapon(this);
        }

        public override void Unapply()
        {
            _visualModel.gameObject.SetActive(false);

            _visualModel.SetParent(transform);
        }

        public virtual void Shoot(bool isShooingStarted)
        {
            if (!isShooingStarted)
                return;

            var ray = new Ray(_shootingPoint.position, transform.forward);
            Debug.DrawLine(ray.origin, ray.GetPoint(50f), Color.red, 3f);

            var raycastHits = Physics.RaycastAll(ray, float.MaxValue, LayerMask.GetMask("Enemies", "Obstacles"));

            var hitResults = raycastHits.ToList();
            hitResults.Sort((x, y) => x.distance.CompareTo(y.distance));

            _muzzleFlashParticleSystem.Play();
            
            for (int i = 0; i < hitResults.Count; ++i)
            {
                // Gameplay code
                if (hitResults[i].collider.gameObject.layer == LayerMask.NameToLayer("Obstacles"))
                {
                    return;
                }

                var characterHealthComponent = hitResults[i].collider.GetComponentInParent<CharacterHealthComponent>();
                if (characterHealthComponent != null)
                {
                    //Debug.Log("Character was hit!");
                    characterHealthComponent.ModifyHealth(-50f);
                }
            }
        }
    }
}
