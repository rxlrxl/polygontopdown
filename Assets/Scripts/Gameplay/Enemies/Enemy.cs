﻿using UnityEngine;

namespace PolygonTopDown
{
    public class Enemy : Character
    {
        [SerializeField] public EnemyType Type = EnemyType.Undefined;
    }
}
