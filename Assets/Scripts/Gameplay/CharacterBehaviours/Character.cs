using UnityEngine;

namespace PolygonTopDown
{
    [RequireComponent(typeof(CharacterPickUpBehaviour))]
    [RequireComponent(typeof(CharacterHealthComponent))]
    [RequireComponent(typeof(CharacterAnimator))]
    public class Character : MonoBehaviour
    {
        public Weapon CurrentWeapon { get; private set; }
        public CharacterInventory Inventory { get { return _inventoryComponent; } }
        
        public Transform RightHandBone { get { return _rightHandBone; } }

        [SerializeField] private CharacterInventory _inventoryComponent = null;
        
        [SerializeField] private Transform _rightHandBone = null;

        private CharacterPickUpBehaviour _pickUpComponent = null;
        private CharacterHealthComponent _healthComponent = null;
        private CharacterAnimator _animatorComponent = null;

        private void Awake()
        {
            _pickUpComponent = GetComponent<CharacterPickUpBehaviour>();
            _healthComponent = GetComponent<CharacterHealthComponent>();
            _animatorComponent = GetComponent<CharacterAnimator>();

            _healthComponent.EventDead += OnCharacterDead;
        }

        private void Start()
        {
            var defaultWeapon = GetComponentInChildren<Weapon>();
            if (defaultWeapon != null)
            {
                _inventoryComponent.PickUp(defaultWeapon);
                defaultWeapon.Apply(this);
            }
        }

        private void OnDestroy()
        {
            // ReSharper disable once DelegateSubtraction
            _healthComponent.EventDead -= OnCharacterDead;
        }

        public void Shoot(bool isShooingStarted)
        {
            if (!isShooingStarted)
                return;
            
            CurrentWeapon.Shoot(isShooingStarted);
            _animatorComponent.SetAttackWeaponAnimation(CurrentWeapon.Type.GetAttackAnimationTriggerName());
        }

        public InventoryItem TryPickUpItem()
        {
            var pickedUpItem = _pickUpComponent.TryPickUpItem();
            if (pickedUpItem != null)
            {
                _inventoryComponent.PickUp(pickedUpItem);
            }

            return pickedUpItem;
        }
        
        private void OnCharacterDead(CharacterHealthComponent healthComponent)
        {
            _animatorComponent.Die();

            GetComponent<Rigidbody>().isKinematic = true;
            
            var colliders = GetComponentsInChildren<Collider>();
            foreach (var collider in colliders)
            {
                collider.enabled = false;
            }
        }
        
        public void ApplyWeapon(Weapon weapon)
        {
            if (CurrentWeapon != null)
            {
                CurrentWeapon.Unapply();
            }

            CurrentWeapon = weapon;
            
            // Start new idle animation
            _animatorComponent.SetIdleWeaponAnimation(CurrentWeapon.Type.GetIdleAnimationTriggerName());
            _animatorComponent.LeftHandIKTarget = CurrentWeapon.LeftHandIKTarget;
        }
    }
}
