﻿using UnityEngine;

namespace PolygonTopDown
{
    [RequireComponent(typeof(Character))]
    [RequireComponent(typeof(CharacterMovementBehaviour))]
    public class PlayerCharacterController : CharacterController
    {
        private CharacterMovementBehaviour _movementBehaviour = null;

        private void Awake()
        {
            Character = GetComponent<Character>();
            _movementBehaviour = GetComponent<CharacterMovementBehaviour>();

            InputManager.Instance.EventPlayerMovementDirectionChanged += OnPlayerMovementDirectionChanged;
            InputManager.Instance.EventPlayerShootingOccured += OnPlayerShootingOccured;
            InputManager.Instance.EventPickUpItemButtonPressed += OnPickUpItemButtonPressed;
        }

        private void OnDestroy()
        {
            if (InputManager.TryInstance != null)
            {
                InputManager.Instance.EventPickUpItemButtonPressed -= OnPickUpItemButtonPressed;
                InputManager.Instance.EventPlayerShootingOccured -= OnPlayerShootingOccured;
                InputManager.TryInstance.EventPlayerMovementDirectionChanged -= OnPlayerMovementDirectionChanged;
            }
        }

        // DEBUG
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.U))
            {
                Debug.Log("Equipping next weapon in inventory");
                var weapons = Character.Inventory.Items.FindAll(i => i.GetType() == typeof(Weapon));
                var currentWeaponIndex = weapons.IndexOf(Character.CurrentWeapon);
                var nextWeapon = weapons[(currentWeaponIndex + 1) % weapons.Count];
                nextWeapon.Apply(Character);
            }

            if (Input.GetKeyDown(KeyCode.I))
            {
                var item = Character.Inventory.Items.Find(i => i != Character.CurrentWeapon);
                if (item != null)
                {
                    Debug.Log("Dropping item");
                    Character.Inventory.Drop(item);
                }
            }
            
            if (Input.GetKeyDown(KeyCode.H))
            {
                Character.GetComponent<CharacterHealthComponent>().ModifyHealth(-10f);
            }
        }
        // end DEBUG

        private void OnPlayerMovementDirectionChanged(Vector3 targetDirection)
        {
            _movementBehaviour.ChangeCharacterMovementDirection(targetDirection);
        }
        
        private void OnPlayerShootingOccured(bool isShooingStarted)
        {
            Character.Shoot(isShooingStarted);
        }
        
        private void OnPickUpItemButtonPressed()
        {
            var pickedUpItem = Character.TryPickUpItem();
        }
    }
}
