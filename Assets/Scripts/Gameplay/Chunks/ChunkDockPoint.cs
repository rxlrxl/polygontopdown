﻿using UnityEngine;

namespace PolygonTopDown
{
    public class ChunkDockPoint : MonoBehaviour
    {
        [SerializeField] public ChunkDockPointType Type = ChunkDockPointType.Undefined;
    }
}
