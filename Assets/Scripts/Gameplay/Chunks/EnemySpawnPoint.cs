﻿using UnityEngine;

namespace PolygonTopDown
{
    public class EnemySpawnPoint : MonoBehaviour
    {
        [SerializeField] public EnemyType EnemyType = EnemyType.Undefined;
    }
}
