namespace PolygonTopDown
{
    public enum ChunkDockPointType
     {
         Undefined = 0,
         
         TypeAWide = 1,
         TypeANarrow = 2,
         
         TypeASlopeUpWide = 3,
         TypeASlopeUpNarrow = 4,
         
         TypeASlopeDownWide = 5,
         TypeASlopeDownNarrow = 6,
     }
 }
