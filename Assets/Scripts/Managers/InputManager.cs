using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace PolygonTopDown
{
    public class InputManager : SingletonGameObject<InputManager>
    {
        public Action<Vector3> EventPlayerMovementDirectionChanged;
        public Action<bool> EventPlayerShootingOccured;
        public Action EventPickUpItemButtonPressed;
        
        private Vector3 _targetMovementVector = Vector3.zero;
        
        private void Update()
        {
            var newVelocity = Vector3.zero;

            if (Input.GetKey(KeyCode.W))
            {
                newVelocity += Vector3.forward;
            }

            if (Input.GetKey(KeyCode.S))
            {
                newVelocity += -Vector3.forward;
            }

            if (Input.GetKey(KeyCode.D))
            {
                newVelocity += Vector3.right;
            }

            if (Input.GetKey(KeyCode.A))
            {
                newVelocity += -Vector3.right;
            }

            if (Input.GetKeyDown(KeyCode.F))
            {
                EventPickUpItemButtonPressed?.Invoke();
            }

            if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
                EventPlayerShootingOccured?.Invoke(true);
            if (Input.GetMouseButtonUp(0) && !EventSystem.current.IsPointerOverGameObject())
                EventPlayerShootingOccured?.Invoke(false);
            
            newVelocity = newVelocity.normalized;

            if (_targetMovementVector != newVelocity)
            {
                _targetMovementVector = newVelocity;
                EventPlayerMovementDirectionChanged?.Invoke(_targetMovementVector);
            }
        }
    }
}
