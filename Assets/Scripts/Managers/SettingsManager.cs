﻿using System.Collections.Generic;
using UnityEngine;

namespace PolygonTopDown
{
    public class SettingsManager : SingletonGameObject<SettingsManager>
    {
        [SerializeField] public List<Chunk> Chunks = null;
        [SerializeField] public List<Enemy> Enemies = null;
    }
}
