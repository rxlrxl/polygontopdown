﻿namespace PolygonTopDown
{
    public enum UIPanelType
    {
        Undefined = 0,

        Gameplay = 1,
        Inventory = 2,
        Pause = 3
    }
}
