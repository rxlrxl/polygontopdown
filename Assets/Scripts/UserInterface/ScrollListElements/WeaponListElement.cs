﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace PolygonTopDown
{
    public class WeaponListElement : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        public Weapon Weapon { get; private set; }
        
        [SerializeField] private CanvasGroup _canvasGroup = null;
        [SerializeField] private Text _weaponTypeLabel = null;

        private Transform _initialParentTransform = null;

        public void SetInfo(Weapon weapon)
        {
            Weapon = weapon;
            
            _weaponTypeLabel.text = Weapon.Type.ToString();
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            _initialParentTransform = transform.parent;
            
            transform.SetParent(GetComponentInParent<Canvas>().transform);
            _canvasGroup.blocksRaycasts = false;
        }
        
        public void OnDrag(PointerEventData eventData)
        {
            transform.position = eventData.position;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            _canvasGroup.blocksRaycasts = true;
            if (transform.parent == GetComponentInParent<Canvas>().transform)
            {
                transform.SetParent(_initialParentTransform);
                transform.localPosition = Vector3.zero;
            }
        }
    }
}
