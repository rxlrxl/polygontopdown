using UnityEngine;

namespace PolygonTopDown
{
    public class PausePanel : UIPanel
    {
        private void OnEnable()
        {
            Time.timeScale = 0.0f;
        }

        private void OnDisable()
        {
            Time.timeScale = 1.0f;
        }
        
        public void OnResumeButtonClick()
        {
            UIManager.Instance.ShowPanel(UIPanelType.Gameplay);
        }
    }
}
