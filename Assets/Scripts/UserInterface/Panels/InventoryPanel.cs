﻿using System.Collections.Generic;
using UnityEngine;

namespace PolygonTopDown
{
    public class InventoryPanel : UIPanel
    {
        [SerializeField] private WeaponListElement _weaponListElementPrefab = null;

        [SerializeField] private RectTransform _listContentParent = null;

        [SerializeField] private CurrentWeaponInventorySlot _currentWeaponInventorySlot = null;

        private Character _character = null;
        
        private Dictionary<InventoryItem, WeaponListElement> _itemToListElementDictionary =
            new Dictionary<InventoryItem, WeaponListElement>();

        private void Awake()
        {
            var playerCharacterController = FindObjectOfType<PlayerCharacterController>();
            _character = playerCharacterController.Character;
            _character.Inventory.EventItemPickedUp += OnPlayerPickedUpItem;
            _character.Inventory.EventItemDroppedDown += OnPlayerDroppedDownItem;

            _currentWeaponInventorySlot.EventWeaponElementAssigned += OnCurrentWeaponAssigned;

            foreach (var item in _character.Inventory.Items)
            {
                var spawnedElement = SpawnInventoryItemListElement(item);
                
                if (spawnedElement.Weapon == _character.CurrentWeapon)
                {
                    _currentWeaponInventorySlot.AssignWeaponElement(spawnedElement);
                }
            }
        }

        private void OnEnable()
        {
            Time.timeScale = 0.0f;
        }

        private void OnDisable()
        {
            Time.timeScale = 1.0f;
        }

        private void OnDestroy()
        {
            var playerCharacterController = FindObjectOfType<PlayerCharacterController>();
            if (playerCharacterController != null)
            {
                playerCharacterController.Character.Inventory.EventItemPickedUp -= OnPlayerPickedUpItem;
                playerCharacterController.Character.Inventory.EventItemDroppedDown -= OnPlayerDroppedDownItem;    
            }
            
            _currentWeaponInventorySlot.EventWeaponElementAssigned -= OnCurrentWeaponAssigned;
        }

        private WeaponListElement SpawnInventoryItemListElement(InventoryItem item)
        {
            var spawnedElement = Instantiate(_weaponListElementPrefab);
            spawnedElement.transform.SetParent(_listContentParent);
            spawnedElement.SetInfo((Weapon)item);
            
            _itemToListElementDictionary.Add(item, spawnedElement);

            return spawnedElement;
        }

        private void RemoveInventoryItemListElement(InventoryItem item)
        {
            Destroy(_itemToListElementDictionary[item].gameObject);
            _itemToListElementDictionary.Remove(item);
        }

        private void OnPlayerPickedUpItem(InventoryItem item)
        {
            SpawnInventoryItemListElement(item);
        }
        
        private void OnPlayerDroppedDownItem(InventoryItem item)
        {
            RemoveInventoryItemListElement(item);
        }

        private void OnCurrentWeaponAssigned(WeaponListElement assignedWeaponElement, WeaponListElement previousWeaponElement)
        {
            if (assignedWeaponElement == previousWeaponElement)
                return;
            
            assignedWeaponElement.Weapon.Apply(_character);

            previousWeaponElement.transform.SetParent(_listContentParent);
        }
        
        public void OnResumeButtonClick()
        {
            UIManager.Instance.ShowPanel(UIPanelType.Gameplay);
        }
    }
}
